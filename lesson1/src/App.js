import React from 'react';
import HomePage from './container/Home';

function App() {
  return (
    <HomePage />
  );
}

export default App;
