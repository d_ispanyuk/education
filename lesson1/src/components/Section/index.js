import React, { Component } from 'react';
import "./Section.css"

export default class Section extends Component {
  render() {
    const {sectionTitle, children, sectionBackground} = this.props
    return (
        <div className={`section ${sectionBackground}`}>
            <h2 className='section__title'>{sectionTitle}</h2>
            {children}
        </div>
    );
  }
}