import React, { Component } from 'react';
import './Header.css';

export default class Header extends Component {

    static headerLinks = [
        {
            name: "Home",
            link: "#",
            id: 1,
        },
        {
            name: "About",
            link: "#",
            id: 2,
        },
        {
            name: "Services",
            link: "#",
            id: 3,
        },
        {
            name: "Contact",
            link: "#",
            id: 4,
        }
    ]

  render() {
    return (
        <header>
        <nav>
            <div className="menu">
            <div className="logo">
                <a href="#">CodingLab</a>
            </div>
            <ul>
                {
                    Header.headerLinks.map((item) => <li key={item.id}><a href={item.link}>{item.name}</a></li>)
                }
            </ul>
            </div>
        </nav>
        </header>
    )
  }
}
