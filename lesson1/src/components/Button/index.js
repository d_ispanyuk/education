import React, { Component } from 'react';
import "./Button.css"

export default class Button extends Component {
  render() {
    const {children, ...propsData} = this.props
    return (
        <button className='appButton' {...propsData}>
            {children}
        </button>
    );
  }
}
