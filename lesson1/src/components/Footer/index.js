import React, { Component } from 'react';
import "./Footer.css"

export default class Footer extends Component {
  render() {
    return (
        <div className='footer'>
            <div className='footer__content'>
                <a href='#' className='footer__link'>Terms & Conditions</a>
                <a href='#' className='footer__link'>Privacy & Policy</a>
            </div>
        </div>
    );
  }
}
