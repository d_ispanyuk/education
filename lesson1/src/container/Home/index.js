import React, { Component } from 'react';
import Header from '../../components/Header';
import Footer from '../../components/Footer';
import Section from '../../components/Section/index';
import ContentImage from "../../assets/img/about-content.jpg"
import Input from '../../components/Input/index';
import TextArea from '../../components/Textarea/index';
import Button from '../../components/Button/index';
import "./HomePage.css"
export default class HomePage extends Component {
  render() {
    return (
    <div>
        <Header />
        <Section
          sectionBackground="center"
        >
            <div className='center__content'>
              <div className="center__title">Create Amazing Website</div>
              <div className="center__sub-title">Pure HTML & CSS Only</div>
              <div className="center__buttons">
              <Button>
                Learn more
              </Button>
              <Button onClick={this.sayHello}>
                Subscribe
              </Button>
              </div>
            </div>
        </Section>
        <Section
          sectionTitle="About us"
          sectionBackground="about"
        >
          <div className='about-content'>
            <div className='about-content__image'>
              <img src={ContentImage} alt="image" />
            </div>
            <p className='about-content__text'>
              Occaecat commodo incididunt est anim nostrud duis incididunt pariatur. Culpa esse veniam do sint occaecat aliqua id aliqua dolor labore quis anim aute. Elit adipisicing laboris laborum non nulla officia aliqua.
            </p>
          </div>
        </Section>
        <Section
          sectionTitle="Services"
          sectionBackground="services"
        >
          <div className='services-content'>
              <p className='services-content__text'>
                Occaecat commodo incididunt est anim nostrud duis incididunt pariatur. Culpa esse veniam do sint occaecat aliqua id aliqua dolor labore quis anim aute. Elit adipisicing laboris laborum non nulla officia aliqua.
              </p>
            </div>
        </Section>
        <Section
          sectionTitle="Contacts"
          sectionBackground="contact"
        >
          <form className='contact__form'>
            <div className='contact__field'>
                <Input
                    placeholder="Your email"
                    autoComplete='off'
                    type="email"
                    name="email"
                />
            </div>
            <div className='contact__field'>
                <Input
                    placeholder="Your name"
                    name="userName"
                    autoComplete='off'
                    type="text"
                />
            </div>
            <div className='contact__field'>
                <TextArea
                    placeholder='Your message'
                    name="userMessage"
                    autoComplete='off'
                />
            </div>
            <div className='contact__field'>
                <Button>
                    Send
                </Button>
            </div>
          </form>
        </Section>
        <Footer />
    </div>);
  }
}
