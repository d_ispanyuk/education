export const themeContext = {
    white: "#fff",
    black: "#000",
    semiBlack: "#000003b0",
    primary: "#7DA2A9",
    error: "#D8000C",
    shadowForm: "0 0 2px 0px",
}