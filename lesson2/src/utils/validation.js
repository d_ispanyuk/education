export function validation(userValue, userType, userPass='') {
    if(!userValue){
        return "Required field"
    }
    if(userType === "userEmail"){
        const conditionOfValidationEmail = userValue.toLowerCase().match(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
        if(!conditionOfValidationEmail){
            return "Invalid value"
        }else{
            return ''
        }
    }

    if(userType === "userAge"){
        if(userValue < 1){
            return "Invalid value"
        }
        return ""
    }

    if(userType === "userFirstName" || userType === "userLastName"){
        if(userValue.length < 2){
            return "Name should be more then 2 symbols"
        }
        if(/\d/.test(userValue)){
            return "Invalid value"
        }
        return ""
    }
    if(userType === "userRepeatPassword"){
        if(!(userValue === userPass)){
            return "Password don't match"
        }
        return ''
    }
}