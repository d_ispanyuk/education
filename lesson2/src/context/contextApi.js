import React, { useState } from 'react';
import signUpContext from '../container/Auth/AuthContext/contextApi';

export const MyContext = React.createContext({});

export default function Provider({children, data}) {
  const [signUpFields, setSignUpFields] = useState(signUpContext)


  return (
      <MyContext.Provider value={{setSignUpFields, signUpFields, data}}>
        {children}
      </MyContext.Provider>
    );
}

