import styled from "styled-components";
import authBack from "../../assets/images/auth-back.jpg"

export const AuthStyleContainer = styled.div`
    width: 100%;
    position: relative;
    background-image: url(${authBack});
    background-size: cover;
    background-position: center;
    background-repeat: no-repeat;
    min-height: 100vh;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    font-family: 'Nunito', sans-serif;
    z-index: 1;
    &:after{
        content: "";
        position: absolute;
        width:100%;
        height: 100%;
        background: ${props => props.semiBlack};
    }
`
export const AuthWrapper = styled.div`
    z-index: 2;
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    margin: 0 20px;
`
