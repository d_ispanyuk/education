const signUpContext = {
    values: {
        userFirstName: '',
        userLastName: '',
        userAge: '',
        userEmail: '',
        userPassword: '',
        userRepeatPassword: '',
    },
    errors:{
        userFirstName: '',
        userLastName: '',
        userAge: '',
        userEmail: '',
        userPassword: '',
        userRepeatPassword: '',
    },
    loader: false,
    accessSend: false,
};

export default signUpContext;