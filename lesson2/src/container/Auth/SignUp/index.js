import React, { useContext } from "react";
import * as Styled from "./style";
import FormField from "../../../components/FormField";
import { validation } from "../../../utils/validation";
import { AppLoader } from "../../../components/Loader/index";
import AuthContainer from "../../../components/AuthContainer/index";
import { MyContext } from "../../../context/contextApi";

const fieldsConfig = [
  {
    label: "First Name",
    inputName: "userFirstName",
    placeHolder: "Enter your name",
    inputType: "text",
    id: 1,
  },
  {
    label: "Last Name",
    inputName: "userLastName",
    placeHolder: "Enter your surname",
    inputType: "text",
    id: 2,
  },
  {
    label: "Age",
    inputName: "userAge",
    placeHolder: "Enter your age",
    inputType: "number",
    id: 3,
  },
  {
    label: "Email",
    inputName: "userEmail",
    placeHolder: "Enter email",
    inputType: "text",
    id: 4,
  },
  {
    label: "Password",
    inputName: "userPassword",
    inputType: "password",
    placeHolder: "Enter password",
    id: 5,
  },
  {
    label: "Repeat Password",
    inputName: "userRepeatPassword",
    placeHolder: "Repeat password",
    inputType: "password",
    id: 6,
  },
];

export default function SignUp() {
  const { signUpFields, setSignUpFields } = useContext(MyContext);

  const handleSubmit = () => {
    setSignUpFields((prevElem) => {
      return { ...prevElem, loader: true };
    });
    const getAuthKeys = Object.keys(signUpFields.values);
    const fieldErrors = {};
    for (let i = 0; i < getAuthKeys.length; i++) {
      fieldErrors[getAuthKeys[i]] = validation(
        signUpFields.values[getAuthKeys[i]],
        getAuthKeys[i],
        signUpFields.values["userPassword"]
      );
    }

    checkValidation(getAuthKeys, fieldErrors);
  };

  const checkValidation = (getKeys, getErrorArr) => {
    for (let i = 0; i < getKeys.length; i++) {
      if (getErrorArr[getKeys[i]]) {
        setSignUpFields((prevElem) => {
          return {
            ...prevElem,
            errors: { ...prevElem.errors, ...getErrorArr },
            accessSend: false,
            loader: false,
          };
        });
        return;
      }
    }
    setTimeout(() => 
      setSignUpFields((prevElem) => {
        return {
          ...prevElem,
          errors: { ...prevElem.errors, getErrorArr },
          accessSend: true,
          loader: false,
        };
      }
    ), 2500)
  };

  return signUpFields.loader ? (
    <>
      <AppLoader />
    </>
  ) : (
    <>
      {!signUpFields.accessSend && (
        <AuthContainer title="Register" clickFunc={handleSubmit}>
          <Styled.AuthForm autocomplete="off">
            {fieldsConfig.map((field) => (
              <FormField
                key={field.id}
                inputValue={signUpFields[field.inputName]}
                inputChange={(elem) =>
                  setSignUpFields((prev) => {
                    return {
                      ...prev,
                      values: {
                        ...prev.values,
                        [field.inputName]: elem.target.value,
                      },
                    };
                  })
                }
                {...field}
              >
                {signUpFields.errors[field.inputName] ? (
                  <Styled.ErrorMessage>
                    {signUpFields.errors[field.inputName]}
                  </Styled.ErrorMessage>
                ) : null}
              </FormField>
            ))}
          </Styled.AuthForm>
        </AuthContainer>
      )}
      {signUpFields.accessSend && (
        <>
          <Styled.AuthSuccess>Welcome!</Styled.AuthSuccess>
        </>
      )}
    </>
  );
}
