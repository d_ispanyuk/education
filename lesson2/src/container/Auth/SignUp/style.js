import styled from "styled-components"

export const AuthForm = styled.form`
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
`

export const ErrorMessage = styled.p`
    margin: 0;
    padding: 0;
    margin-top: 5px;
    margin-left: 5px;
    font-size: 18px;
    font-weight: bold;
    color: #D8000C;
`
export const AuthSuccess = styled.p`
    font-size: 35px;
    text-align: center;
    padding: 10px 20px;
    background-color: #7DA2A9;
    border-radius: 5px;
    color: #fff;
`