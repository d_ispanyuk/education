import React, { useContext } from "react";
import SignUp from "./SignUp";
import * as Styled from "./style";
import { ThemeContext } from "../../App";
import Provider from "../../context/contextApi";

export default function Auth() {
  const theme = useContext(ThemeContext);
  return (
    <Styled.AuthStyleContainer {...theme}>
      <Styled.AuthWrapper>
        <Provider>
          <SignUp />
        </Provider>
      </Styled.AuthWrapper>
    </Styled.AuthStyleContainer>
  );
}
