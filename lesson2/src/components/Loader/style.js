import styled from "styled-components"
import { Puff } from 'react-loader-spinner';

export const AppLoaderWrapper = styled(Puff)`
    z-index: 2;
`