import React from "react";
import { Puff } from "react-loader-spinner";

export const AppLoader = () => {
  return <Puff color="#fff" height={100} width={100} />;
};
