import React, { useContext } from "react";
import * as Styled from "./style";
import AppButton from "../Button/index";
import { ThemeContext } from "../../App";
export default function AuthContainer({ children, title, clickFunc }) {
  const theme = useContext(ThemeContext);
  return (
    <>
      <Styled.AuthTitle {...theme}>{title}</Styled.AuthTitle>
      <Styled.AuthContent {...theme}>{children}</Styled.AuthContent>
      <Styled.AuthSend>
        <AppButton clickFunc={clickFunc}>
          <Styled.AuthArrow {...theme} />
        </AppButton>
      </Styled.AuthSend>
    </>
  );
}
