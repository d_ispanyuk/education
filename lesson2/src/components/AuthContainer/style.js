import styled from "styled-components";

export const AuthContent = styled.div`
    max-width: 500px;
    background: ${props => props.primary};
    padding: 10px 20px;
    border-radius: 5px;
    width: 100%;
    z-index: 2;
    box-shadow: ${props => props.shadowForm};
`

export const AuthTitle = styled.h2`
    margin: 0;
    padding: 0;
    color: ${props => props.white};
    font-size: 30px;
    margin-bottom: 20px;
`

export const AuthSend = styled.div`
    position: absolute;
    bottom: 20px;
    right: 20px;
`

export const AuthArrow = styled.p`
    display: flex;
    margin: 0;
    position: relative;
    width: 30px;
    height: 4px;
    background-color: ${props => props.primary};
    border-radius: 5px;
    &:after{
        content: "";
        position: absolute;
        width: 10px;
        right: 0;
        transform: rotate(45deg);
        top: -4px;
        height: 10px;
        border: 3px solid ${props => props.primary};
        border-radius: 3px;
        border-left: none;
        border-bottom: none;
    }
`