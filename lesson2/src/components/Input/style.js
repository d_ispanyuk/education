import styled from "styled-components"

export const InputStyled = styled.input`
    outline: none;
    font-family: 'Nunito', sans-serif;
    border-radius: 5px;
    font-size: 16px;
    padding: 10px;
    border:none;
`