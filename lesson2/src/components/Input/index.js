import React from "react";
import * as Styled from "./style";

export default function AppInput({
  placeHolder,
  inputChange,
  inputType,
}) {
  return (
    <Styled.InputStyled
      autoComplete="off"
      placeholder={placeHolder}
      type={inputType}
      onChange={inputChange}
    />
  );
}
