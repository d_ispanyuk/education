import styled from "styled-components"

export const FormField = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: left;
    width: 100%;
    margin: 7px 0;
`
export const FormLabel = styled.label`
    font-size: 18px;
    font-weight: bold;
    margin-bottom: 5px;
    margin-left: 5px;
    color: ${props => props.white};
`