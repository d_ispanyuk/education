import React, { useContext } from "react";
import AppInput from "../Input";
import * as Styled from "./style";
import { ThemeContext } from "../../App";

export default function FormField({
  children,
  label,
  placeHolder,
  inputChange,
  inputType,
}) {
  const themeConfig = useContext(ThemeContext);
  return (
    <Styled.FormField>
      <Styled.FormLabel {...themeConfig}>{label}</Styled.FormLabel>
      <AppInput
        inputChange={inputChange}
        placeHolder={placeHolder}
        inputType={inputType}
      />
      {children}
    </Styled.FormField>
  );
}
