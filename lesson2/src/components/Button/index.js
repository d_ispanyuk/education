import React from "react";
import * as Styled from "./style";

export default function AppButton({ children, clickFunc }) {
  return (
    <Styled.ButtonStyled onClick={clickFunc}>{children}</Styled.ButtonStyled>
  );
}
