import styled from "styled-components"

export const ButtonStyled = styled.button`
    width: 100%;
    height: 100%;
    border: none;
    border-radius: 50px;
    padding: 18px;
    cursor: pointer;
    font-weight: bold;
`