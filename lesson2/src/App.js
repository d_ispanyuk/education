import React from 'react';
import Auth from './container/Auth/index';
import { themeContext } from './theme/style';

export const ThemeContext = React.createContext(themeContext);

export function App() {
  return (
    <ThemeContext.Provider value={themeContext}>
      <Auth />
    </ThemeContext.Provider>
  );
}
